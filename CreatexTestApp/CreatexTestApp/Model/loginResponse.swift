
class loginResponse: Decodable {
    var login: String = ""
    var token: String = ""
    var userId: Int = 0
}
