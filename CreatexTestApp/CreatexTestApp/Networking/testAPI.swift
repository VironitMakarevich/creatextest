

import Moya

enum testAPI {
    case signUP(login: String, password: String)
    case signIn(login: String, password: String)
}

extension testAPI: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "http://junior.balinasoft.com/api") else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .signUP:
            return "account/signup"
        case .signIn:
            return "account/signin"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .signUP:
            return .post
        case .signIn:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .signUP(let login, let password):
            return .requestParameters(parameters: ["login": login, "password": password], encoding: JSONEncoding.default)
        case .signIn(let login, let password):
            return .requestParameters(parameters: ["login": login, "password": password], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .signUP, .signIn:
            return ["Accept": "application/json"]
        }
    }
}

