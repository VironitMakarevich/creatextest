
import Moya

class NetworkManager {
    var token = ""
    let provider = MoyaProvider<testAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    func singUP(login: String, password: String, completion: @escaping (loginResponse) -> ()) {
        provider.request(.signUP(login: login, password: password)) { result in
            switch result {
            case let .success(response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let responseResult = try JSONDecoder().decode(loginResponse.self, from: response.data)
                    self.token = responseResult.token
                    completion(responseResult)
                } catch let error {
                    switch error {
                    case .statusCode:
                        print("dasd")
                    case defaut:
                        print("default")
                    }
                    print("-----Error of encoding ----- \(error)")
                }
            case .failure:
                print("-----Error of connection----- ")
            }
        }
    }
    
    func singIn(login: String, password: String, completion: @escaping (loginResponse) -> ()) {
        provider.request(.signIn(login: login, password: password)) { result in
            switch result {
            case let .success(response):
                do {
                    let responseResult = try JSONDecoder().decode(loginResponse.self, from: response.data)
                    self.token = responseResult.token
                    completion(responseResult)
                } catch let error {
                    print("-----Error of encoding ----- \(error)")
                }
            case .failure:
                print("-----Error of connection----- ")
            }
        }
    }
}
